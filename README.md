# Music player

## Note that this is no longer a webscraping application! I did not change the repository so therefore the url and folder name is still web-scraper

### How to run:
First make sure that you have an installation of vlc media player, preferebly in the path `C:\\Program Files\\VideoLAN\\VLC\\vlc.exe` if not there are going to be issues as this is the path `Audio.hs` expects.

If you do not have VLC downloaded, you can find the installer in the `Extra` folder

After this you can use `stack run` on in the folder and it should start running. Further details are provided in the report. You should be able to navigate the program with only the command-line functions you are prompted by the program.